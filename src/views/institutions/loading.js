import {Spin} from 'view-design'

const loadingUtils = { 
 show() {
    console.log('loading show')
  Spin.show({
    render: h => {
        return h('div', [
            h('Icon', {
                'class': 'demo-spin-icon-load',
                props: {
                    type: 'ios-loading',
                    size: 18
                }
            }),
            h('div', '加载中...')
        ])
        }
    })
 },
 hide(){
    console.log('loading hide')
    Spin.hide()
 }
}
export default loadingUtils