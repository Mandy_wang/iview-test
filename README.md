## 1.[Table 表格](http://v1.iviewui.com/components/table)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/190922_7a6bd468_1787933.png "屏幕截图.png")

- table中组件渲染
- 多层嵌套渲染

## 2.[Upload 上传](http://v1.iviewui.com/components/upload)

```html
<Upload
:headers="auths"
:action="userImport"
:show-upload-list="false"
:format="['xls','csv','xlsx']"
:on-success="afterImport">
    <Button type="primary"  style="background-color: #fff;color:#204DF2;border-color:#204DF2"><icon custom="iconfont icon-shangchuan"  style="margin-right:12px"></icon>上传信息</Button>
</Upload>
```

## 3.下载表格数据api

```js
// 下载数据
export const downLoad = () => {
  return axios.request({
    url: '/api/user/download',
    method: 'get',
    responseType: 'blob'  // 这个是必须的
  })
}
```
## 4.table复选框多选

1. 在 data赋值后添加  

   ```js
   this.data.forEach(item => {
       item._checked = this.checkList.some(it => it.id == item.id)
   })
   // 点击分页进入时判断当前数据是否已勾选  勾选的显示勾选
   ```

2. table 点击事件 on-selection-change 方法中添加

   ```js
   Select (sel) {
         this.checkList = this.checkList.filter(item => !this.data.some(it => it.id == item.id)) // 过滤掉id没有选中的数据
         this.checkList.push(...sel)  // 选中数组合并到选中列表中
       },
   ```

3. 复选框中已选中数据禁止点击

   ```js
   if(item.is_exit ){
       obj['_disabled'] = true
   } else{
       obj['_disabled'] = false
   }
   return obj  // 遍历数据中的每个元素，每个元素已经使用过的_disabled为true 否则为false
   ```

## 5.使用slot代替render函数

```html
<Table border :columns="columns12" :data="data6">
    <template slot-scope="{ row }" slot="name">
        <strong>{{ row.name }}</strong>
    </template>
    <template slot-scope="{ row, index }" slot="action">
        <Button type="primary" size="small" style="margin-right: 5px" @click="show(index)">View</Button>
        <Button type="error" size="small" @click="remove(index)">Delete</Button>
    </template>
</Table>
```

js中内容

```js
columns12: [
    {
        title: 'Name',
        slot: 'name'
    },
    {
        title: 'Age',
        key: 'age'
    },
    {
        title: 'Address',
        key: 'address'
    },
    {
        title: 'Action',
        slot: 'action',
        width: 150,
        align: 'center'
    }
],
```

## 6.加载更多
![输入图片说明](https://images.gitee.com/uploads/images/2021/0419/144806_c4b6ea73_1787933.png "屏幕截图.png")
源码：https://gitee.com/Mandy_wang/iview-test/tree/master/src/views/institutions


```html
<span @click="loadMore" v-if="isShowAll" class="btn">
    加载更多
    <Icon type="md-arrow-round-down" class="icon" />
</span>  
<!--isShowAll: false, // 判断是否已显示完所有的数据-->
```

```js
loadMore() {
    this.switchList = true    //  switchList:false,//是否加载更多
    this.pageNum += 1
    this.showNext() 
},
 // 删除时  this.switchList = false    
        
```

```js
showNext () {
    let list = []
    let params = {
        page:this.pageNum,
        page_size: this.pageSize,
    }
    getDatalist(params).then(res=>{
        list = res.data
        if (this.switchList) {
            // 加载更多时
            this.son.push(...list)
        } else {
            this.son = list
        }
        if (this.son.length < res.data.data.total) { // 判断当前数据小于总数时
            this.isShowAll = true  // 继续显示加载按钮
        } else {
            this.isShowAll = false // 不显示加载按钮
        }
    })
},
```

